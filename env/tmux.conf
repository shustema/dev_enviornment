# ln -s <this_file> ~/.tmux.conf
# remap prefix to C-a
unbind M-b
set-option -g prefix M-a
bind-key M-a send-prefix

# split panes using | and -
bind c new-window       -c "#{pane_current_path}"
bind | split-window -h  -c "#{pane_current_path}"
bind - split-window -v  -c "#{pane_current_path}"
unbind '"'
unbind %

set -sg escape-time 0
set -g status-left '#{pane_current_path}'
setw -g window-status-format "#{pane_current_path}"
setw -g mode-keys vi

# switch panes using Alt-arrow without prefix
bind -n M-h select-pane -L
bind -n M-l select-pane -R
bind -n M-k select-pane -U
bind -n M-j select-pane -D

# Move pane with Control (no prefix)
bind-key -n M-H select-pane -L \; swap-pane -s '!'
bind-key -n M-L select-pane -R \; swap-pane -s '!'
bind-key -n M-K swap-pane -U
bind-key -n M-J swap-pane -D

bind-key -n M-Q kill-pane

# don't rename windows automatically
set-option -g allow-rename off

# Enable mouse mode (tmux 2.1 and above)
set -g mouse on
# set `focus-events`

set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
if "test ! -d ~/tools/progs/tmux/plugins/tmux-resurrect" \
   "run 'git clone https://github.com/tmux-plugins/tmux-resurrect ~/tools/progs/tmux/plugins/tmux-resurrect'"
run '~/tools/progs/tmux/plugins/tmux-resurrect/resurrect.tmux'
if "test ! -d ~/tools/progs/tmux/plugins/tmux-continuum" \
   "run 'git clone https://github.com/tmux-plugins/tmux-continuum ~/tools/progs/tmux/plugins/tmux-continuum'"
run '~/tools/progs/tmux/plugins/tmux-continuum/continuum.tmux'

## restore vim sessions
set -g @resurrect-strategy-vim 'session'

## restore nvim sessions
set -g @ressurect-strategy-nvim 'sessions'

## restore panes 
set -g @resurrect-capture-pane-contents 'on'

## restore last saved enviornment (automatically)
set -g @continuum-restore 'on'

set -g @continuum-boot 'on'
