#--------------------------------------------------------------------- 
# Function
#--------------------------------------------------------------------- 

# Tmux ------------------------------------------------------------------------ 
if [ -n "$SSN_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  if which tmux >/dev/null 2>&1; then
    if [[ -z "$TMUX" ]]; then
      ID="$( tmux ls | grep -vm1 attatched | cut -d: -f1 )"
      if [[ -z "$ID" ]]; then
        tmux new-session
      else
        tmux attach-session -t "$ID"
      fi
    fi
  fi
fi

removelink() {
    [ -L "$1" ] && cp --remove-destination "$(readlink "$1")" "$1"
}
