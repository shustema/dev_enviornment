#------------------------------------------------------------------------ 
# Alias 
#------------------------------------------------------------------------ 

alias lt='ls -ltr'
alias la='ls -al'
alias tl='tail -f'
alias dsize="du -h . --max-depth=1"
alias rm='rm -f'
alias rmdir='rm -rf'
alias mkdir='mkdir -p'

alias calc='bc -l'
alias resource='source ~/.bashrc'
alias rtmux='tmux source-file ~/tools/resources/continuum.tmux'

