let g:ConqueTerm_Color = 2
let g:ConqueTerm_CloseOnEnd = 1
let g:ConqueTerm_StartMessages = 0

" function DebugSession()
"   silent make -o vimgdb -gcflags "-N -l"
"   redraw!
"   if (filereadable("vimgdb"))
"     ConqGdb vimgdb
"   else
"     echom "Couldn't find debug file" 
"   endif
" endfunction
" function DebugSessionCleanup(term)
"   if(filereadable("vimgdb"))
"     let ds=delete("vimgdb")
"   endif
" endfunction
" call conque_term#register_function("after_close", "DebugSessionCleanup")
" nmap <leader>d :call DebugSession()<CR>;

function! MyTermGdb()
  set scrolloff=20
  "autocmd! CursorLine
  packadd termdebug
  tnoremap <Esc> <C-W>N
  noremap <silent> <space>gu :call term_sendkeys(4, 'up' . "\n")<CR>
  noremap <silent> <space>gd :call term_sendkeys(4, 'down' . "\n")<CR>
  noremap <silent> <space>gl :call term_sendkeys(4, 'adv ' . line('.') . "\n")<CR> 
  noremap <silent> <space>gt :call term_sendkeys(4, 'tbreak ' . line('.') . "\n")<CR> 
  noremap <silent> <space>gc :Continue<CR> 
  noremap <silent> <space>gb :Break<CR> 
  noremap <silent> <space>gx :Delete<CR> 
  noremap <silent> <space>gs :Step<CR> 
  noremap <silent> <space>gn :Over<CR> 
  noremap <silent> <space>gf :Finish<CR> 
  noremap <silent> <space>gS :Stop<CR> 
  noremap <silent> <Leader>q :qa!<CR>
  noremap q a
  CocDisable
  Termdebug
  wincmd J
  wincmd k
  wincmd H
  wincmd l
  wincmd j
  wincmd J
  call term_sendkeys(4, 'so gdb' . "\n") 
  Gdb
endfunction

if !has('nvim')
  noremap <silent> <space>gg :call MyTermGdb()<CR>
endif
