" --------------------------------------------------
"  Toggle the splits and maximize a split
" --------------------------------------------------
function! MaximizeToggle()
  if exists("s:maximize_session")
    exec "source " . s:maximize_session
    call delete(s:maximize_session)
    unlet s:maximize_session
    let &hidden=s:maximize_hidden_save
    unlet s:maximize_hidden_save
  else
    let s:maximize_hidden_save = &hidden
    let s:maximize_session = tempname()
    set hidden
    exec "mksession! " . s:maximize_session
    only
  endif
endfunction

" --------------------------------------------------
"  Call Bracket Pairs
" --------------------------------------------------
function! s:CloseBracket()
  let line = getline('.')
  if line =~# '^\s*\(struct\|class\|enum\) '
    return "{\<Enter>};\<Esc>0"
  elseif searchpair('(', '', ')', 'bmn', '', line('.'))
    return "{\<Enter>});\<Esc>0"
  else
    return "{\<Enter>}\<Esc>0"
  endif
endfunction
