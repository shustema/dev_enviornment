let g:EasyMotion_do_mapping = 0 " Disable default mappings 
let g:EasyMotion_smartcase = 1 " Turn on case-insensitive feature

" highlight colors
hi link EasyMotionTarget Search
hi link EasyMotionTarget2First Search
hi link EasyMotionTarget2Second Search
hi link EasyMotionShade Comment

" Jump to anywhere with key
nmap s <Plug>(easymotion-overwin-f)
nmap s <Plug>(easymotion-overwin-f2)

" Move to line j / k 
map <leader>j <Plug>(easymotion-j)
map <leader>k <Plug>(easymotion-k)

