#!/bin/bash

rln () {
  [[ -e $2 ]] && rm -rf $2 2>/dev/null
  ln -s $1 $2 2>/dev/null
}
