#!/bin/bash 

######################################################################
# script to initialize new linux system
######################################################################

##############################################################
# get needed functions
source scripts/defaults.sh
source scripts/rln.sh

##############################################################
# variable declarations
vim_dir=$src_dir/vim

##############################################################
# set up basic links to config files
rln $env_dir/bashrc $home_dir/.bashrc
rln $env_dir/tmux.conf $home_dir/.tmux.conf
rln $env_dir/vimrc $home_dir/.vimrc

##############################################################
# setup vim stuff
[[ ! -d $vim_dir ]] && mkdir $vim_dir 2>/dev/null
rln $env_dir/vim/coc-settings.json $vim_dir/coc-settings.json
rln $env_dir/vim/plug-config $vim_dir/plug-config

##############################################################
# get er done
source $home_dir/.bashrc
